package com.example.demo1.controller;

import com.example.demo1.entity.User;
import org.junit.Test;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RestController
@RequestMapping("/login")
public class LoginController {

    @GetMapping("/login")
    public Map<String,Object> login(){
        Map<String,Object> result = new HashMap<>();
        result.put("zhangsan","123456");
        return result;
    }


}
