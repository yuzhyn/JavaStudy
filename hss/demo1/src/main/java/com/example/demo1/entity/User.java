package com.example.demo1.entity;

import lombok.Data;
import org.junit.Test;
import org.springframework.util.StringUtils;

import java.util.Optional;

import static org.junit.Assert.assertEquals;

@Data
public class User {
    private String userName;
    private String userPhone;
    private int userAge;
    private int userGender;

    public User(String name, String phone, int age, int gender) {
        this.userName = name;
        this.userPhone = phone;
        this.userAge = age;
        this.userGender = gender;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        //判断非空
        if (obj == null) {
            return false;
        }
        //内存地址相等
        if (this == obj) {
            return true;
        }
        if (obj instanceof User) {
            //为空时判断
            boolean isSameEmptyName = StringUtils.isEmpty(this.userName) && StringUtils.isEmpty(((User) obj).userName);
            boolean isSameEmptyPhone = StringUtils.isEmpty(this.userPhone) && StringUtils.isEmpty(((User) obj).userPhone);
            //非空时判断
            boolean isSameName = !StringUtils.isEmpty(this.userName) && !StringUtils.isEmpty(((User) obj).userName) && this.userName.equals(((User) obj).userName);
            boolean isSamePhone = !StringUtils.isEmpty(this.userPhone) && !StringUtils.isEmpty(((User) obj).userPhone) && this.userPhone.equals(((User) obj).userPhone);

            return (isSameEmptyName && isSameEmptyPhone) || (isSameName && isSamePhone) || (isSameEmptyName && isSamePhone) || (isSameName && isSameEmptyPhone);
        }
        return false;
    }

    @Override
    public String toString() {
        return "User{" +
                "userName='" + userName + '\'' +
                ", userPhone='" + userPhone + '\'' +
                ", userAge=" + userAge +
                ", userGender=" + userGender +
                '}';
    }
}
