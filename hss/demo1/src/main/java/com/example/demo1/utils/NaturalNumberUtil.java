package com.example.demo1.utils;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class NaturalNumberUtil {

    /**
     * 判断数字是否为质数（质数又称素数。一个大于1的自然数，除了1和它自身外，不能被其他自然数整除的数叫做质数；否则称为合数。）
     *
     * @param number
     * @return
     */
    public boolean IsPrimeNumber(int number) {
        boolean result = true;
        //1 不是质数
        if (number == 1) {
            result = false;
        }
        //验证是否能被整除（除了1和它自身）
        for (int i = 2; i < number; i++) {
            //能被整除，不是质数，跳出循环
            if (number % i == 0) {
                result = false;
                break;
            }
        }
        return result;
    }

    /**
     * 获取数字区间的质数集合
     * @param begin
     * @param end
     * @return
     */
    public List<Integer> GetPrimeList(int begin, int end) {
        List<Integer> result = new ArrayList<>();
        //0和1既不是质数也不是合数，排除在外
        if (begin < 2) {
            begin = 2;
        }
        //如果数字区间有效（开始自然数小于等于结束自然数），则开始计算，否则返回空列表
        if (begin <= end) {
            for (int number = begin; number <= end; number++) {
                //判断是质数则添加到列表中
                if (IsPrimeNumber(number)) {
                    result.add((number));
                }
            }
        }
        //返回结果列表
        return result;
    }
}
