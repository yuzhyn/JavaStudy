package com.example.demo1.entity;

public class HashCodeUtil {
    /**
     * hashcode计算
     *
     * @param a 对象数组
     * @param number 计算参数
     * @return
     */
    public static int hashCode(char[] a, int number) {
        if (a == null) {
            return 0;
        }
        int result = 1;
        for (Object element : a) {
            result = number * result + (element == null ? 0 : element.hashCode());
        }
        return result;
    }
}
