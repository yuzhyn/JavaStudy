package com.example.demo1.utils;

import com.example.demo1.entity.User;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@Data
public class UserLoyaltyUtil {
    public UserLoyaltyUtil() {
        map = new HashMap<>();
    }

    private Map<User, Integer> map;

    public Integer set(User user) {
        //创建随机数
        Random random = new Random();
        Integer loyalty = random.nextInt(101);
        map.put(user, loyalty);
        return loyalty;
    }

    public Integer get(User user) {
        return map.get(user);
    }
}
