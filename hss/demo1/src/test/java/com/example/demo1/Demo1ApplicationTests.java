package com.example.demo1;

import com.example.demo1.entity.HashCodeUtil;
import com.example.demo1.entity.User;
import com.example.demo1.utils.NaturalNumberUtil;
import com.example.demo1.utils.UserLoyaltyUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.*;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

@SpringBootTest
class Demo1ApplicationTests {

    @Test
    public void TestIsPrime() {
        NaturalNumberUtil util = new NaturalNumberUtil();
        assertEquals(true, util.IsPrimeNumber(3));
    }

    @Test
    public void TestPrimeList() {
        NaturalNumberUtil util = new NaturalNumberUtil();
        List<Integer> list = Arrays.asList(2, 3, 5, 7);
        System.out.println("数字范围内的质数有：" + util.GetPrimeList(0, 10).toString());
        assertEquals(list, util.GetPrimeList(0, 10));
    }

    @Test
    public void TestEquals1() {
        User user1 = new User(null, null, 18, 1);
        User user2 = new User(null, null, 18, 1);
        assertEquals(true, user1.equals(user2));
    }

    @Test
    public void TestEquals2() {
        User user1 = new User(null, "123456", 18, 1);
        User user2 = new User("张三", "123456", 18, 1);
        assertEquals(true, user1.equals(user2));
    }

    @Test
    public void TestEquals3() {
        User user1 = new User("张三", null, 18, 1);
        User user2 = new User("张三", "123456", 18, 1);
        assertEquals(true, user1.equals(user2));
    }

    @Test
    public void TestEquals4() {
        User user1 = new User("张三1", "123456", 18, 1);
        User user2 = new User("张三2", "123456", 18, 1);
        assertEquals(true, user1.equals(user2));
    }

    @Test
    public void TestEquals5() {
        User user1 = new User("张三", "1234567", 18, 1);
        User user2 = new User("张三", "1234568", 18, 1);
        System.out.println(user1);
        assertEquals(true, user1.equals(user2));
    }

    @Test
    public void TestUserLoyaltyUtil() {
        User user1 = new User("张三", "123456", 18, 1);
        User user2 = new User("张三", "123456", 18, 1);
        UserLoyaltyUtil util = new UserLoyaltyUtil();
        //设置user1到hashmap中，然后通过同样参数的user2获取忠诚度
        Integer loyalty1 = util.set(user1);
        Integer loyalty2 = util.get(user2);
        //获取user1和user2的hashcode，两个对象虽然数据相同，但是hashcode并不相同
        int hashCode1 = user1.hashCode();
        int hashCode2 = user2.hashCode();
        //结论：hashcode计算和之前的其他重载并没有影响用user2获取不到user1的忠诚度
        assertEquals(loyalty1, util.get(user2));
    }

    @Test
    public void TestHashCrash() {
        //读取字典文件，这里根据自己电脑路径配置！！！！！！！！！！
        File file = new File("e:\\dict2.txt");
        //定义列表存储所有的hashCode值，方便判断碰撞情况
        List<Integer> hashCodeList = new ArrayList<>();
        //声明文件读取对象
        BufferedReader reader = null;
        //声明文件写入对象
        FileWriter writer = null;
        try {
            //实例化文件读取和写出对象，这里根据自己电脑设置写出文件路径！！！！！！！！！！
            reader = new BufferedReader(new FileReader(file));
            writer = new FileWriter("e:\\" + UUID.randomUUID().toString() + ".txt");
            //自定义计算参数，原本计算参数在计算函数中，这里提取出来作为参数传递，自行调整，查看测试结果！！！！！！！！！！
            int hashCodeNumber = 199;
            //写出文本文件结果
            writer.write("hashCode 计算参数：" + hashCodeNumber + "\r\n");
            String s;
            //记录开始时间
            LocalDateTime beginTime = LocalDateTime.now();
            while ((s = reader.readLine()) != null) {
                //逐行读取字典的文字，并计算hashcode值
                int hashCode = HashCodeUtil.hashCode(s.toCharArray(), hashCodeNumber);
                //判断列表中是否存在该值，存在则为碰撞情况发生
                boolean isHashCrash = hashCodeList.contains(hashCode);
                if (isHashCrash) {
                    //记录发生碰撞
                    writer.write(s + " " + hashCode + " hashCode 重复 " + "\r\n");
                } else {
                    //如果列表中不存在该值，则暂存到列表中，方便之后判断
                    hashCodeList.add(hashCode);
                }
            }
            //记录结束时间，并写出到文本文件
            LocalDateTime endTime = LocalDateTime.now();
            Duration duration = Duration.between(beginTime, endTime);
            writer.write("总运行时长：" + duration.toMillis() + " ms");
            //清空文本写入缓冲区数据
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //释放文件读取对象
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            //释放文件写入对象
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        System.out.println("函数运行完毕");
    }
}
